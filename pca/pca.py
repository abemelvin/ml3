import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.stats import kurtosis
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

    data = pd.read_csv('../datasets/' + dataset + '.csv')
    scaler = StandardScaler()
    X = data.drop(["class"], axis=1).copy().astype(np.float64)
    X = scaler.fit_transform(X)
    y = data["class"].copy()


    pca = PCA()
    mod_data = pca.fit_transform(X)
    plt.figure()
    plt.plot(range(1, X.shape[1]+1), np.cumsum(pca.explained_variance_ratio_))
    plt.xticks(range(1, X.shape[1]+1))
    plt.grid()
    plt.xlabel('Number of Components')
    plt.ylabel('Cumulative Variance')
    plt.title('PCA: ' + dataset)
    plt.savefig(dataset + '/pca_variance.png')
    plt.close()

    cov_matrix = np.dot(X.T, X) / X.shape[0]
    eigenvalues = pca.explained_variance_
    plt.figure()
    plt.plot(range(1, X.shape[1]+1), eigenvalues)
    plt.xticks(range(1, X.shape[1]+1))
    plt.grid()
    plt.xlabel('Component')
    plt.ylabel('Eigenvalues (Descending)')
    plt.title('Eigenvalues: ' + dataset)
    plt.savefig(dataset + '/pca_eigenvalues.png')
    plt.close()


    for i in range(mod_data.shape[1]):
        mu, sigma = norm.fit(mod_data[:, i])
        kurt = kurtosis(mod_data[:, i])
        plt.figure()
        plt.xlim(mu - (4*sigma), mu + (4*sigma))
        hist, bins, patches = plt.hist(mod_data[:, i], bins=int(mod_data[:, i].shape[0]/10), density=True)
        plt.title(r'$\mathrm{PCA\ Component\ Histogram:}\ \sigma=%.3f,\ \beta_2=%.3f$' %(sigma, kurt))
        plt.xlabel('Component ' + str(i+1) + ' Values')
        plt.ylabel('Frequency')
        plt.savefig(dataset + '/component_' + str(i+1) + '.png')
        plt.close()