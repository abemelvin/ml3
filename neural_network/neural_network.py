import time
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier
from scipy.stats import norm
from scipy.stats import kurtosis
from sklearn.cluster import KMeans
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from sklearn.random_projection import SparseRandomProjection
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

    data = pd.read_csv('../datasets/' + dataset + '.csv')
    scaler = StandardScaler()
    X = data.drop(["class"], axis=1).copy().astype(np.float64)
    y = data["class"].copy()
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, shuffle=True)
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    pca_time = []
    ica_time = []
    rp_time = []
    feat_agglo_time = []

    for schema in ['pca', 'ica', 'rp', 'feat_agglo']:

        train_acc = []
        test_acc = []

        for n in range(1, X_train.shape[1]+1):

            np.random.seed(4)

            if schema == 'pca':
                pca = PCA(n_components=n)
                mod_data = pca.fit_transform(X_train)
                mod_test = pca.transform(X_test)
            elif schema == 'ica':
                ica = FastICA(n_components=n, max_iter=5000)
                mod_data = ica.fit_transform(X_train)
                mod_test = ica.transform(X_test)
            elif schema == 'rp':
                rp = SparseRandomProjection(n_components=n)
                mod_data = rp.fit_transform(X_train)
                mod_test = rp.transform(X_test)
            elif schema == 'feat_agglo':
                agglo = FeatureAgglomeration(n_clusters=n)
                mod_data = agglo.fit_transform(X_train)
                mod_test = agglo.transform(X_test)
            else:
                mod_data = X_train


            # plot learning curves using best model parameters
            # https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html#sphx-glr-auto-examples-model-selection-plot-learning-curve-py
            #title = "Learning Curves (MLP): " + dataset + ' - ' + schema + ': ' + n
            estimator = MLPClassifier(hidden_layer_sizes=(10, 10, 10), alpha=0.01, max_iter=2000)
            t0 = time.time()
            estimator.fit(mod_data, y_train)
            mlp_fit = time.time() - t0
            train_acc.append(estimator.score(mod_data, y_train))
            test_acc.append(estimator.score(mod_test, y_test))
            if schema == 'pca': pca_time.append(mlp_fit)
            elif schema == 'ica': ica_time.append(mlp_fit)
            elif schema == 'rp': rp_time.append(mlp_fit)
            elif schema == 'feat_agglo': feat_agglo_time.append(mlp_fit)
            else: pass
            #plt.savefig(dataset + "/mlp_learning_curve_" + schema + '_' + n + ".png")
            #plt.close()

        estimator = MLPClassifier(hidden_layer_sizes=(10, 10, 10), alpha=0.01, max_iter=2000)
        t0 = time.time()
        estimator.fit(X_train, y_train)
        mlp_fit = time.time() - t0
        train_score = estimator.score(X_train, y_train)
        test_score = estimator.score(X_test, y_test)

        plt.title('Accuracy vs Components: ' + schema)
        plt.ylabel('Accuracy')
        plt.xlabel('Number of Components')
        plt.plot(range(1, X_train.shape[1]+1), train_acc, label='train')
        plt.plot(range(1, X_train.shape[1]+1), test_acc, label='test')
        plt.hlines(train_score, 1, X_train.shape[1]+1, linestyles='dashed', colors='blue', label='original train')
        plt.hlines(test_score, 1, X_train.shape[1]+1, linestyles='dashed', colors='orange', label='original test')
        plt.legend(loc='best')
        plt.xticks(range(1, X_train.shape[1]+1))
        plt.savefig(dataset + '/accuracy_' + schema + '.png')
        plt.close()

    plt.plot(range(1, X_train.shape[1]+1), pca_time, label='pca')
    plt.plot(range(1, X_train.shape[1]+1), ica_time, label='ica')
    plt.plot(range(1, X_train.shape[1]+1), rp_time, label='rp')
    plt.plot(range(1, X_train.shape[1]+1), feat_agglo_time, label='feat_agglo')
    plt.hlines(mlp_fit, 1, X_train.shape[1]+1, linestyles='dashed', label='original')
    plt.xticks(range(1, X_train.shape[1]+1))
    plt.legend(loc='best')
    plt.title('Training Time Comparison: ' + dataset)
    plt.xlabel('Number of Components')
    plt.ylabel('Time (s)')
    plt.savefig(dataset + '/train_time.png')
    plt.close()

"""
    for algo in ['k_means', 'gmm']:

        pca_time = []
        ica_time = []
        rp_time = []
        feat_agglo_time = []

        for schema in ['pca', 'ica', 'rp', 'feat_agglo']:

            data = pd.read_csv('../datasets/' + dataset + '.csv')
            clusters = pd.read_csv(dataset + '/' + algo + '_' + schema +'.csv')
            scaler = StandardScaler()
            X = data.drop(["class"], axis=1).copy().astype(np.float64)
            X = scaler.fit_transform(X)
            y = data["class"].copy()

            train_acc = []
            test_acc = []

            for n in range(1, X.shape[1]+1):

                np.random.seed(4)

                if schema == 'pca':
                    pca = PCA(n_components=n)
                    mod_data = pca.fit_transform(X)
                elif schema == 'ica':
                    ica = FastICA(n_components=n, max_iter=5000)
                    mod_data = ica.fit_transform(X)
                elif schema == 'rp':
                    rp = SparseRandomProjection(n_components=n)
                    mod_data = rp.fit_transform(X)
                elif schema == 'feat_agglo':
                    agglo = FeatureAgglomeration(n_clusters=n)
                    mod_data = agglo.fit_transform(X)
                else:
                    mod_data = X


                X = pd.concat([pd.DataFrame(mod_data), clusters], axis=1, sort=False)
                X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, shuffle=True)
                np.random.seed(8)

                estimator = MLPClassifier(hidden_layer_sizes=(10, 10, 10), alpha=0.01, max_iter=2000)
                t0 = time.time()
                estimator.fit(X_train, y_train)
                mlp_fit = time.time() - t0
                train_acc.append(estimator.score(X_train, y_train))
                test_acc.append(estimator.score(X_test, y_test))
                if schema == 'pca': pca_time.append(mlp_fit)
                elif schema == 'ica': ica_time.append(mlp_fit)
                elif schema == 'rp': rp_time.append(mlp_fit)
                elif schema == 'feat_agglo': feat_agglo_time.append(mlp_fit)
                else: pass

            estimator = MLPClassifier(hidden_layer_sizes=(10, 10, 10), alpha=0.01, max_iter=2000)
            t0 = time.time()
            estimator.fit(X_train, y_train)
            mlp_fit = time.time() - t0
            train_score = estimator.score(X_train, y_train)
            test_score = estimator.score(X_test, y_test)

            plt.title('Accuracy vs Components: ' + schema)
            plt.ylabel('Accuracy')
            plt.xlabel('Number of Components')
            plt.plot(range(1, X_train.shape[1]), train_acc, label='train')
            plt.plot(range(1, X_train.shape[1]), test_acc, label='test')
            plt.hlines(train_score, 1, X_train.shape[1], linestyles='dashed', colors='blue', label='original train')
            plt.hlines(test_score, 1, X_train.shape[1], linestyles='dashed', colors='orange', label='original test')
            plt.legend(loc='best')
            plt.xticks(range(1, X_train.shape[1]))
            plt.savefig(dataset + '/' + algo + '_' + schema + '_accuracy.png')
            plt.close()

        plt.plot(range(1, X_train.shape[1]), pca_time, label='pca')
        plt.plot(range(1, X_train.shape[1]), ica_time, label='ica')
        plt.plot(range(1, X_train.shape[1]), rp_time, label='rp')
        plt.plot(range(1, X_train.shape[1]), feat_agglo_time, label='feat_agglo')
        plt.hlines(mlp_fit, 1, X_train.shape[1], linestyles='dashed', label='original')
        plt.xticks(range(1, X_train.shape[1]))
        plt.legend(loc='best')
        plt.title('Training Time Comparison: ' + dataset)
        plt.xlabel('Number of Components')
        plt.ylabel('Time (s)')
        plt.savefig(dataset + '/' + algo + '_train_time.png')
        plt.close()
"""