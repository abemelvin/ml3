import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.stats import kurtosis
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

    data = pd.read_csv('../datasets/' + dataset + '.csv')
    scaler = StandardScaler()
    X = data.drop(["class"], axis=1).copy().astype(np.float64)
    X = scaler.fit_transform(X)
    y = data["class"].copy()

    error = []

    for i in range(1, X.shape[1]+1):

        agglo = FeatureAgglomeration(n_clusters=i)
        mod_data = agglo.fit_transform(X)
        re_data = agglo.inverse_transform(mod_data)
        mse = mean_squared_error(X, re_data)
        error.append(mse)

    plt.plot(range(1, X.shape[1]+1), error)
    plt.xticks(range(1, X.shape[1]+1))
    plt.grid()
    plt.xlabel('Number of Components')
    plt.ylabel('Reconstruction Error')
    plt.title('Feature Agglomeration: ' + dataset)
    plt.savefig(dataset + '/feat_agglo_reconstruction_error.png')
    plt.close()