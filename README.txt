Repository link: https://bitbucket.org/abemelvin/ml3/src/master/

There is a separate folder in this repository for every major algorithm explored.

In each of those folders, there are subfolders for outputting results. There is also
a driver script that executes the experiments associated with that algorithm and outputs
the artifacts into the respective data set subfolders.

If you truly want to attempt reproducing all of the artifacts, the scripts should be run
in this order:

pca.py from within /pca
ica.py from within /ica
rp.py from within /rp
feat_agglo.py from within /feat_agglo
k_means.py from within /k_means
gmm.py from within /gmm
neural_network.py from within /neural_network
neural_network_clusters.py from within /neural_network

The order matters as some of the data used by the later scripts is produced by the earlier
scripts.

If code has been yoinked, I have copy-pasted the corresponding web link in an adjacent comment.
The libraries utilized have been listed in the acknowledgements section of the report.