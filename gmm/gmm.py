import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.stats import kurtosis
import itertools
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import FeatureAgglomeration
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from sklearn.random_projection import SparseRandomProjection
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

        data = pd.read_csv('../datasets/' + dataset + '.csv')
        scaler = StandardScaler()
        X = data.drop(["class"], axis=1).copy().astype(np.float64)
        X = scaler.fit_transform(X)
        y = data["class"].copy().astype(np.int64)
        #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, shuffle=True)
        #X_train = scaler.fit_transform(X_train)
        #X_test = scaler.transform(X_test)

        for schema in ['original', 'pca', 'ica', 'rp', 'feat_agglo']:

                if schema == 'pca':
                        if dataset == 'eeg_eye_state':
                                pca = PCA(n_components=5)
                                mod_data = pca.fit_transform(X)
                        else:
                                pca = PCA(n_components=10)
                                mod_data = pca.fit_transform(X)
                elif schema == 'ica':
                        if dataset == 'eeg_eye_state':
                                ica = FastICA(n_components=5)
                                mod_data = ica.fit_transform(X)
                        else:
                                ica = FastICA(n_components=7)
                                mod_data = ica.fit_transform(X)
                elif schema == 'rp':
                        rp = SparseRandomProjection(n_components=X.shape[1])
                        mod_data = rp.fit_transform(X)
                elif schema == 'feat_agglo':
                        if dataset == 'eeg_eye_state':
                                agglo = FeatureAgglomeration(n_clusters=10)
                                mod_data = agglo.fit_transform(X)
                        else:
                                agglo = FeatureAgglomeration(n_clusters=9)
                                mod_data = agglo.fit_transform(X)
                else: mod_data = X


                # https://scikit-learn.org/stable/auto_examples/mixture/plot_gmm_selection.html#sphx-glr-auto-examples-mixture-plot-gmm-selection-py
                bic = []
                lowest_bic = np.inf
                log_sph = []
                log_tied = []
                log_diag = []
                log_full = []
                n_components_range = range(1, 21)
                cv_types = ['spherical', 'tied', 'diag', 'full']
                
                for clusters in n_components_range:
                        for cv_type in cv_types:

                                gmm = GaussianMixture(n_components=clusters, covariance_type=cv_type)
                                gmm.fit(mod_data)
                                bic.append(gmm.bic(mod_data))
                                if cv_type == 'spherical': log_sph.append(gmm.score(mod_data))
                                elif cv_type == 'tied': log_tied.append(gmm.score(mod_data))
                                elif cv_type == 'diag': log_diag.append(gmm.score(mod_data))
                                elif cv_type == 'full': log_full.append(gmm.score(mod_data))
                                else: pass

                                if bic[-1] < lowest_bic:
                                        lowest_bic = bic[-1]
                                        best_gmm_bic = gmm

                print("Best model (BIC, doesn't work very well):", best_gmm_bic.n_components, "components,", best_gmm_bic.covariance_type, "covariance... " + dataset)
                bic = np.array(bic)
                color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue', 'darkorange'])
                bars = []
                plt.figure()

                for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
                        xpos = np.array(n_components_range) + 0.2 * (i - 2)
                        bars.append(plt.bar(xpos, bic[i * len(n_components_range):(i + 1) * len(n_components_range)], width=0.2, color=color))

                plt.xticks(n_components_range)
                plt.title('BIC Score per Model: ' + dataset)
                plt.xlabel('Number of Components')
                plt.legend([b[0] for b in bars], cv_types)
                plt.grid()
                plt.savefig(schema + '/' + dataset + '/bic.png')
                plt.close()

                plt.figure()
                plt.plot(n_components_range, log_sph, label='spherical')
                plt.plot(n_components_range, log_tied, label='tied')
                plt.plot(n_components_range, log_diag, label='diag')
                plt.plot(n_components_range, log_full, label='full')
                plt.xticks(n_components_range)
                plt.grid()
                plt.title('Log Likelihood Score per Model: ' + dataset)
                plt.xlabel('Number of Components')
                plt.legend(loc='best')
                plt.savefig(schema + '/' + dataset + '/log_likelihood.png')
                plt.close()

                if schema == 'pca':
                        if dataset == 'eeg_eye_state':
                                n=6
                                cv_type='full'
                        else:
                                n=10
                                cv_type='full'
                elif schema == 'ica':
                        if dataset == 'eeg_eye_state':
                                n=5
                                cv_type='full'
                        else:
                                n=9
                                cv_type='full'
                elif schema == 'rp':
                        if dataset == 'eeg_eye_state':
                                n=5
                                cv_type='full'
                        else:
                                n=10
                                cv_type='full'
                elif schema == 'feat_agglo':
                        if dataset == 'eeg_eye_state':
                                n=5
                                cv_type='full'
                        else:
                                n=8
                                cv_type='full'
                else:
                        if dataset == 'eeg_eye_state':
                                n=5
                                cv_type='full'
                        else:
                                n=14
                                cv_type='full'

                estimator = GaussianMixture(n_components=n, covariance_type=cv_type)
                estimator.fit(mod_data)
                labels = estimator.predict(mod_data)

                if schema != 'original':
                        pd.DataFrame(labels).to_csv('../neural_network/' + dataset + '/gmm_' + schema + '.csv', index=False)

                counts = []

                for cluster in range(n):
                        ctr = 0
                        for idx, label in enumerate(labels):
                                if label == cluster: ctr += 1
                        counts.append(ctr)
                
                classes = ['cluster_' + str(i) for i in range(1, n+1)]
                plt.pie(counts, labels=counts, autopct='%1.1f%%')
                plt.title('Cluster Coverage: ' + dataset)
                plt.legend(classes, loc='best')
                plt.savefig(schema + '/' + dataset + '/coverage.png')
                plt.close()

                for cluster in range(n):
                        pos = 0
                        neg = 0
                        for idx, label in enumerate(labels):
                                if label == cluster:
                                        if y.iloc[idx] == 1: pos += 1
                                        else: neg += 1
                        classes = ['class_1', 'class_0']
                        sizes = [pos, neg]
                        plt.pie(sizes, labels=sizes, autopct='%1.1f%%')
                        plt.title('Cluster ' + str(cluster+1) + ' Class Representation: ' + dataset)
                        plt.legend(classes, loc='best')
                        plt.savefig(schema + '/' + dataset + '/cluster_' + str(cluster+1) + '.png')
                        plt.close()