import pandas as pd
import numpy as np
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.random_projection import SparseRandomProjection
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

    data = pd.read_csv('../datasets/' + dataset + '.csv')
    scaler = StandardScaler()
    X = data.drop(["class"], axis=1).copy().astype(np.float64)
    X = scaler.fit_transform(X)
    y = data["class"].copy()

    mean = []
    var = []

    for i in range(1, X.shape[1]+1):

        sample = []
        for j in range(500):
            rp = SparseRandomProjection(n_components=i)
            mod_data = rp.fit_transform(X)
            #re_data = ((np.linalg.pinv(rp.components_) @ rp.components_) @ X_train.T).T
            re_data = ((np.linalg.pinv(rp.components_.todense()) @ rp.components_.todense()) @ X.T).T
            mse = mean_squared_error(X, re_data)
            sample.append(mse)
        (mu, sigma) = norm.fit(sample)
        mean.append(mu)
        var.append(sigma)

    plt.errorbar(range(1, X.shape[1]+1), mean, yerr=sigma)
    plt.xticks(range(1, X.shape[1]+1))
    plt.grid()
    plt.xlabel('Number of Components')
    plt.ylabel('Reconstruction Error')
    plt.title('Random Projection: ' + dataset)
    plt.savefig(dataset + '/rp_reconstruction_error.png')
    plt.close()