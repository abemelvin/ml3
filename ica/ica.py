import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.stats import kurtosis
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.decomposition import FastICA
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

    data = pd.read_csv('../datasets/' + dataset + '.csv')
    scaler = StandardScaler()
    X = data.drop(["class"], axis=1).copy().astype(np.float64)
    X = scaler.fit_transform(X)
    y = data["class"].copy()

    error = []

    for i in range(1, X.shape[1]+1):

        ica = FastICA(n_components=i, max_iter=5000)
        mod_data = ica.fit_transform(X)
        re_data = ica.inverse_transform(mod_data)
        mse = mean_squared_error(X, re_data)
        error.append(mse)

    plt.plot(range(1, X.shape[1]+1), error)
    plt.xticks(range(1, X.shape[1]+1))
    plt.grid()
    plt.xlabel('Number of Components')
    plt.ylabel('Reconstruction Error')
    plt.title('ICA: ' + dataset)
    plt.savefig(dataset + '/ica_reconstruction_err.png')
    plt.close()

    ica = FastICA(max_iter=2000)
    mod_data = ica.fit_transform(X)
    kurts = []

    for i in range(mod_data.shape[1]):
        mu, sigma = norm.fit(mod_data[:, i])
        kurt = kurtosis(mod_data[:, i])
        kurts.append(abs(3-kurt))
        plt.figure()
        plt.xlim(mu - (4*sigma), mu + (4*sigma))
        hist, bins, patches = plt.hist(mod_data[:, i], bins=int(mod_data[:, i].shape[0]/10), density=True)
        plt.title(r'$\mathrm{ICA\ Component\ Histogram:}\ \sigma=%.3f,\ \beta_2=%.3f$' %(sigma, kurt))
        plt.xlabel('Component ' + str(i+1) + ' Values')
        plt.ylabel('Frequency') 
        plt.savefig(dataset + '/component_' + str(i+1) + '.png')
        plt.close()

    plt.plot(range(1, mod_data.shape[1]+1), sorted(kurts, reverse=True))
    plt.xticks(range(1, mod_data.shape[1]+1))
    plt.yscale('log')
    plt.grid()
    plt.title('ICA Adjusted Kurtosis: ' + dataset)
    plt.xlabel('Component')
    plt.ylabel('Magnitude')
    plt.savefig(dataset + '/log_kurtosis.png')
    plt.yscale('linear')
    plt.savefig(dataset + '/lin_kurtosis.png')
    plt.close()