import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.stats import kurtosis
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from sklearn.random_projection import SparseRandomProjection
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

for dataset in ['eeg_eye_state', 'diabetic_retinopathy']:

        data = pd.read_csv('../datasets/' + dataset + '.csv')
        scaler = StandardScaler()
        X = data.drop(["class"], axis=1).copy().astype(np.float64)
        X = scaler.fit_transform(X)
        y = data["class"].copy().astype(np.int64)
        #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, shuffle=True)
        #X_train = scaler.fit_transform(X_train)
        #X_test = scaler.transform(X_test)

        for schema in ['original', 'pca', 'ica', 'rp', 'feat_agglo']:

                if schema == 'pca':
                        if dataset == 'eeg_eye_state':
                                pca = PCA(n_components=5)
                                mod_data = pca.fit_transform(X)
                        else:
                                pca = PCA(n_components=10)
                                mod_data = pca.fit_transform(X)
                elif schema == 'ica':
                        if dataset == 'eeg_eye_state':
                                ica = FastICA(n_components=5)
                                mod_data = ica.fit_transform(X)
                        else:
                                ica = FastICA(n_components=7)
                                mod_data = ica.fit_transform(X)
                elif schema == 'rp':
                        rp = SparseRandomProjection(n_components=X.shape[1])
                        mod_data = rp.fit_transform(X)
                elif schema == 'feat_agglo':
                        if dataset == 'eeg_eye_state':
                                agglo = FeatureAgglomeration(n_clusters=10)
                                mod_data = agglo.fit_transform(X)
                        else:
                                agglo = FeatureAgglomeration(n_clusters=9)
                                mod_data = agglo.fit_transform(X)
                else: mod_data = X

                inertias = []

                for clusters in range(1, 21):

                        estimator = KMeans(n_clusters=clusters)
                        estimator.fit(mod_data)
                        inertias.append(estimator.inertia_)

                plt.plot(range(1, 21), inertias)
                plt.xticks(range(1, 21))
                plt.title('K-Means Clustering Inertia: ' + dataset)
                plt.grid()
                plt.xlabel('Clusters')
                plt.ylabel('Inertia')
                plt.savefig(schema + '/' + dataset + '/inertia.png')
                plt.close()

                if schema == 'pca':
                        if dataset == 'eeg_eye_state':
                                n=5
                        else:
                                n=8
                elif schema == 'ica':
                        if dataset == 'eeg_eye_state':
                                n=5
                        else:
                                n=7
                elif schema == 'rp':
                        if dataset == 'eeg_eye_state':
                                n=5
                        else:
                                n=10
                elif schema == 'feat_agglo':
                        if dataset == 'eeg_eye_state':
                                n=6
                        else:
                                n=14
                else:
                        if dataset == 'eeg_eye_state':
                                n=5
                        else:
                                n=10

                estimator = KMeans(n_clusters=n)
                estimator.fit(mod_data)
                labels = estimator.labels_
                if schema != 'original':
                        pd.DataFrame(labels).to_csv('../neural_network/' + dataset + '/k_means_' + schema + '.csv', index=False)

                counts = []

                for cluster in range(n):
                        ctr = 0
                        for idx, label in enumerate(labels):
                                if label == cluster: ctr += 1
                        counts.append(ctr)
                
                classes = ['cluster_' + str(i) for i in range(1, n+1)]
                plt.pie(counts, labels=counts, autopct='%1.1f%%')
                plt.title('Cluster Coverage: ' + dataset)
                plt.legend(classes, loc='best')
                plt.savefig(schema + '/' + dataset + '/coverage.png')
                plt.close()

                for cluster in range(n):
                        pos = 0
                        neg = 0
                        for idx, label in enumerate(labels):
                                if label == cluster:
                                        if y.iloc[idx] == 1: pos += 1
                                        else: neg += 1
                        classes = ['class_1', 'class_0']
                        sizes = [pos, neg]
                        plt.pie(sizes, labels=sizes, autopct='%1.1f%%')
                        plt.title('Cluster ' + str(cluster+1) + ' Class Representation: ' + dataset)
                        plt.legend(classes, loc='best')
                        plt.savefig(schema + '/' + dataset + '/cluster_' + str(cluster+1) + '.png')
                        plt.close()